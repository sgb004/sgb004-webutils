/**
 * @name: ImgCpt
 * @author: sgb004
 * @version: 0.4.0
 */
import { ListenerScroll } from '../../utils/listener-scroll';

export class IframeCpt extends HTMLElement {
	listenerScrollId = 0;

	constructor() {
		console.log('PRUEBA');

		super();

		this.shadow = this.attachShadow({ mode: 'open' });

		this.listenerScrollId = ListenerScroll.addEventListenerScroll(
			this.checkStage.bind(this)
		);
	}

	checkStage() {}
}
