/**
 * @name: ImgCpt
 * @author: sgb004
 * @version: 0.4.0
 */
import { ListenerScroll } from '../../utils/listener-scroll';
export class ImgCpt extends HTMLElement {
	picture;
	loading;
	listenerScrollId = 0;
	waitSrcTimer;
	waitHTMLTimer;

	constructor() {
		super();

		this.shadow = this.attachShadow({ mode: 'open' });

		const style = document.createElement('style');
		style.textContent = `:host {display: inline-block; position: relative;} picture{ position: relative;} picture, img { display: block; width: inherit; height: inherit; max-width: inherit; max-height: inherit; border-radius: inherit;}img { width: 100%; height: 100%;} picture, loading-cpt{ opacity: 1; transition-timing-function: ease-out; transition: opacity 1s; } loading-cpt{ position: absolute; top: 0; right: 0; width: 100%; height: 100%; border-radius: inherit; } picture[hide], loading-cpt[hide]{ opacity: 0; }`;
		this.shadow.appendChild(style);

		this.picture = document.createElement('picture');
		this.picture.setAttribute('hide', true);

		this.loading = document.createElement('loading-cpt');
		this.loading.setAttribute('hide', true);

		this.shadow.appendChild(this.loading);
		this.shadow.appendChild(this.picture);

		this.listenerScrollId = ListenerScroll.addEventListenerScroll(
			this.checkStage.bind(this)
		);
	}

	connectedCallback() {
		ListenerScroll.callEventScroll(this.listenerScrollId);
	}

	imgLoad() {
		this.picture.removeAttribute('hide');
		this.loading.setAttribute('hide', true);
		this.setAttribute('loaded', true);
		clearTimeout(this.waitSrcTimer);
	}

	checkStage() {
		if (super.innerHTML == '') {
			this.waitHTMLTimer = setTimeout(this.waitHTML.bind(this), 1);
		} else {
			let ett = this.getBoundingClientRect().top - window.innerHeight;

			if (window.pageYOffset >= ett) {
				this.picture.innerHTML = super.querySelector(
					'picture'
				).innerHTML;
				this.picture.setAttribute('hide', true);
				this.loading.removeAttribute('hide');
				this.waitSource();
				return true;
			}
		}
		return false;
	}

	waitHTML() {
		if (super.innerHTML == '') {
			this.waitHTMLTimer = setTimeout(this.waitHTML.bind(this), 1);
		} else {
			clearTimeout(this.waitHTMLTimer);
			this.connectedCallback();
		}
	}

	waitSource() {
		const source = this.picture.querySelector('source');
		const img = this.picture.querySelector('img');

		if (source.srcset && !img) {
			const sourceSrcset = source.srcset.split(',');
			const imgType = source.getAttribute('imgtype');
			const img = document.createElement('img');
			let imgSrc = sourceSrcset[0].split(' ');
			imgSrc = imgSrc[0];
			imgSrc = imgSrc.split('.');
			imgSrc[imgSrc.length - 1] = imgType == 'image/png' ? 'png' : 'jpg';
			imgSrc = imgSrc.join('.');
			img.src = imgSrc;
			this.picture.appendChild(img);
			this.waitSrcTimer = setTimeout(this.waitSource.bind(this), 1);
		} else if (source.srcset && img) {
			const sources = this.picture.querySelectorAll('source, img');
			for (let i = 0; i < sources.length; i++) {
				sources[i].complete
					? this.imgLoad()
					: (sources[i].onload = this.imgLoad.bind(this));
			}
		} else {
			this.picture.innerHTML = super.querySelector('picture').innerHTML;
			this.waitSrcTimer = setTimeout(this.waitSource.bind(this), 1);
		}
	}

	set innerHTML(html) {
		super.innerHTML = html;
		//ListenerScroll.callEventScroll(this.listenerScrollId);
		this.connectedCallback();
	}

	get innerHTML() {
		return this.picture.innerHTML;
	}
}
